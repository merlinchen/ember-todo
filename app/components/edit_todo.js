/**
 * edit_todo.js
 * Copyright (c) 2013 Youngfriend Inc.
 * All rights reserved.
 */
export default Ember.TextField.extend({
    didInsertElement: function () {
        this.$().focus();
    }
});