/**
 * todos.js
 * Copyright (c) 2013 Youngfriend Inc.
 * All rights reserved.
 */
export default Ember.Route.extend({
    model: function() {
        return this.store.find("todo");
    }
});