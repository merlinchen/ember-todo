/**
 * active.js
 * Copyright (c) 2013 Youngfriend Inc.
 * All rights reserved.
 */
export default Ember.Route.extend({
    model: function(){
        return this.store.filter('todo', function (todo) {
            return !todo.get('isCompleted');
        });
    },
    renderTemplate: function(controller){
        this.render('todos/index', {controller: controller});
    }
});